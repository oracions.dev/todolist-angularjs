# TodoList project

## Requirements
Install cordova globally:
```bash
npm i -g cordova
```

Install JS dependencies:
```bash
yarn
# or
npm i
# It will install dependencies under the node_modules folder
```

Install cordova dependencies:
```bash
cordova prepare
# It will install cordova plugins and will add android and browser platforms
```

## Starting developpment
To start an angular developpment server run:
```bash
npm start
# or 
yarn start
```

## Run with cordova api
### On the browser
```bash
cordova run browser
```

### On your phone
```bash
cordova run android
```

## Build an apk
```bash
cordova build android
```
or if you want to release the app
```bash
cordova build android --release
```
