import * as moment from 'moment';
import { Task } from './Task.class';

export class GroupedTask implements Iterable<[moment.Moment, Task[]]> {

    private dates: Array<{
        date: moment.Moment,
        tasks: Task[]
    }> = [];

    private pointer = 0;

    [Symbol.iterator](): Iterator<[moment.Moment, Task[]]> {
        return this;
    }

    public next(): IteratorResult<[moment.Moment, Task[]]> {

        if (this.pointer < this.dates.length) {
            const { date, tasks } = this.dates[this.pointer];
            return {
                done: false,
                value: [date, tasks]
            };
        } else {
            return {
                done: true,
                value: null
            };
        }
    }

    public add(...tasks: Task[]): void {
        for (const task of tasks) {
            const dateArray = this.getDateArray(task.dueDate || moment());
            for (let i = 0 ; i < dateArray.length ; i++) {
                if (task.priority >= dateArray[i].priority) {
                    dateArray.splice(i, 0, task);
                    return;
                }
            }
            dateArray.push(task);
        }
        console.log(this.dates);
    }

    public remove(task: Task): void {
        const dateArray = this.getDateArray(task.dueDate);
        dateArray.splice(dateArray.indexOf(task), 1);
    }

    public toJSON(): string {
        return JSON.stringify(
            this.dates
                .map(({ tasks }) => tasks)
                .reduce((a, b) => [...a, ...b], [])
        );
    }

    public fromJSON(json: string): void {
        JSON.parse(json)
            .map((rawTask) => new Task(rawTask))
            .forEach((task: Task) => this.add(task));
    }

    private getDateArray(date: moment.Moment): Task[] {
        for (let i = 0 ; i < this.dates.length ; i++) {
            if (date.isSame(this.dates[i].date, 'day')) {
                return this.dates[i].tasks;
            } else if (date.isAfter(this.dates[i].date, 'day')) {
                this.dates.splice(i, 0, { date, tasks: [] });
            }
            return date[i];
        }

        this.dates.push({ date, tasks: [] });
        return this.dates[this.dates.length - 1].tasks;
    }
}
