import * as moment from 'moment';

export abstract class TodoComponent {
    /**
     * Définie les différentes priorités d'une tâche
     */
    public static readonly PRIORITY: {
        [key: number]: string
    } = {
        0: 'aucune',
        1: 'haute',
        2: 'moyenne',
        3: 'basse'
    };

    public id: number;
    public name: string;
    public priority: number;
    public done: boolean;
    public url: string;
    public notes: string;
    private _date: moment.Moment;
    get date() {
        return this._date || moment();
    }
    set date(date: moment.Moment) {
        this._date = date;
    }

    public isDateDefined(): boolean {
        return this._date !== undefined && this._date !== null;
    }

    public toString(): string {
        return JSON.stringify(this);
    }
}
