import { TodoComponent } from './TodoComponent';

export class TodoComposite extends TodoComponent implements Iterable<TodoComponent> {
    
    [Symbol.iterator](): Iterator<TodoComponent> {
        return this.todoList[Symbol.iterator]();
    }
    
    public todoList: TodoComponent[];

    constructor(todoList: TodoComponent[] = []) {
        super();
        this.todoList = todoList;
    }

    public add(newTodo: TodoComponent) {
        let i = 0;
        for (const todo of this.todoList) {
            if (todo.date.isBefore(newTodo.date)) {
                this.todoList.splice(i, 0, newTodo);
                return;
            }
            i++;
        }
        this.todoList.push(newTodo);
    }

    public remove(todo: TodoComponent): void {
        
    }
}

