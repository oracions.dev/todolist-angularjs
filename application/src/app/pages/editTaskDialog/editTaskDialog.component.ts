import { Component, Inject } from '@angular/core';
import { Task } from 'src/app/models/Task.class';
import { TasksService } from 'src/app/services/tasks.service';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { DeleteDialogComponent } from './deleteDialog.component';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-edit-task',
    templateUrl: './editTaskDialog.component.html',
    styleUrls: ['./editTaskDialog.component.scss']
})
export class EditTaskDialogComponent {
    /**
     * Instance de la tâche originel dans le cas d'une modification
     * Lors d'une modification l'originalTask est dupliqué vers la propriété this.task
     * pour ne pas modifier l'instance original avant d'avoir sauvegardé
     */
    public originalTask: Task;

    /**
     * Instance de la nouvelle tâche ou de la tâche avec les valeurs saisies dans les inputs
     */
    public task: Task;

    /**
     * Liste des priorités d'une tâche
     */
    public readonly PRIORITY = Task.PRIORITY;

    constructor(
        private snackBarService: MatSnackBar,
        private dialogService: MatDialog,
        private taskService: TasksService,
        public dialogRef: MatDialogRef<EditTaskDialogComponent>,
        @Inject(MAT_DIALOG_DATA) id: number
    ) {
        console.log(id);
        if (id === -1) {
            this.task = new Task();
        } else {
            this.originalTask = taskService.getById(id);
        }
        this.task = Object.assign({}, this.originalTask);
    }

    public save(): void {
        if (this.originalTask) {
            Object.assign(this.originalTask, this.task);
            this.taskService.save();
        } else {
            console.log(this.task);
            this.taskService.addTask(this.task);
        }
        this.dialogRef.close();
    }

    public delete(): void {
        this.dialogService.open(DeleteDialogComponent)
            .afterClosed()
            .pipe(filter((result) => result))
            .subscribe((result) => {
                this.taskService.deleteTask(this.originalTask);
                this.dialogRef.close();
                this.snackBarService.open('La tâche à été supprimé', null, { duration: 1000 });
            });
    }
}
